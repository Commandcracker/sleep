package oculus.sleep;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class Updater implements Listener {

    private final String currentversion;
    private String latestversion = null;
    private final Sleep plugin = Sleep.getInstance();

    public Updater() {
        currentversion = plugin.getDescription().getVersion();

        Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, () -> {

            URL url = null;
            try {
                url = new URL("https://gitlab.com/Commandcracker/sleep/-/raw/master/pom.xml");
            } catch (MalformedURLException e) {
                Sleep.pluginmessage(ChatColor.DARK_RED + "Update: URL is Broken.");
                e.printStackTrace();
            }

            if (url != null) {
                URLConnection connection = null;
                try {
                    connection = url.openConnection();
                } catch (IOException e) {
                    Sleep.pluginmessage(ChatColor.DARK_RED + "Update: Can't Open Connection.");
                    e.printStackTrace();
                }

                if (connection != null) {
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0");
                    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder dBuilder = null;
                    try {
                        dBuilder = dbFactory.newDocumentBuilder();
                    } catch (ParserConfigurationException e) {
                        Sleep.pluginmessage(ChatColor.DARK_RED + "Update: Parsing failure.");
                        e.printStackTrace();
                    }
                    if (dBuilder != null) {
                        Document doc = null;
                        try {
                            doc = dBuilder.parse((InputStream) connection.getContent());
                        } catch (SAXException | IOException e) {
                            Sleep.pluginmessage(ChatColor.DARK_RED + "Update: Cant get Content from URL.");
                            e.printStackTrace();
                        }
                        if (doc != null) {
                            latestversion = doc.getElementsByTagName("version").item(0).getTextContent();
                        }
                    }
                }
            }
        }, 0L, 20 * 60 * 60 * 24); // run every day


        Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, () -> {
            if (latestversion != null) {
                if (!currentversion.equals(latestversion)) {
                    Sleep.pluginmessage(
                            ChatColor.GRAY +
                                    "A new update for " +
                                    ChatColor.AQUA + plugin.getDescription().getName() +
                                    ChatColor.GRAY + " is available! (" +
                                    ChatColor.YELLOW + currentversion +
                                    " " + ChatColor.GRAY + "-> " +
                                    ChatColor.GREEN + latestversion +
                                    ChatColor.GRAY + ")\nDownload: " +
                                    ChatColor.AQUA + plugin.getDescription().getWebsite() +
                                    ChatColor.GRAY + "."
                    );
                }
            } else {
                Sleep.pluginmessage(ChatColor.DARK_RED + "Could not check for Updates.");
            }
        }, 20L);

    }

    @EventHandler
    public void onjoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        if (p.hasPermission("sleep.notify")) {
            if (latestversion != null) {
                if (!currentversion.equals(latestversion)) {
                    p.sendMessage(
                            ChatColor.GRAY +
                                    "A new update for " +
                                    ChatColor.AQUA + plugin.getDescription().getName() +
                                    ChatColor.GRAY + " is available! (" +
                                    ChatColor.YELLOW + currentversion +
                                    " " + ChatColor.GRAY + "-> " +
                                    ChatColor.GREEN + latestversion +
                                    ChatColor.GRAY + ")\nDownload: " +
                                    ChatColor.AQUA + plugin.getDescription().getWebsite() +
                                    ChatColor.GRAY + "."
                    );
                }
            } else {
                p.sendMessage(ChatColor.DARK_RED + "Could not check for Updates.");
            }
        }
    }
}
