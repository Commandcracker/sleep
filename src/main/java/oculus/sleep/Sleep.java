package oculus.sleep;

import com.earth2me.essentials.Essentials;
import oculus.sleep.eventlisteners.onBedEnter;
import oculus.sleep.eventlisteners.onBedLeave;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

public class Sleep extends JavaPlugin {
    public List<Player> playersSleeping = new ArrayList();
    private static Sleep intance = null;
    private static Essentials Essentials;
    private static boolean hasPlaceholderAPI;

    public static void pluginmessage(String msg) {
        for (Player p : Bukkit.getOnlinePlayers()) {
            if (p.hasPermission("sleep.notify")) {
                Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.AQUA + "[" + getInstance().getDescription().getPrefix() + "] " + msg);
                p.sendMessage(ChatColor.AQUA + "[" + getInstance().getDescription().getPrefix() + "] " + msg);
            }
        }
    }

    public static Sleep getInstance() { return Sleep.intance; }

    @Override
    public void onDisable() { Sleep.intance = null; }

    public void onEnable() {
        new Metrics(this, 7776);
        Sleep.intance = this;

        saveDefaultConfig();
        reloadConfig();

        PluginManager pluginManager = Bukkit.getPluginManager();

        Essentials = (Essentials) pluginManager.getPlugin("Essentials");
        hasPlaceholderAPI = pluginManager.getPlugin("PlaceholderAPI") != null;

        pluginManager.registerEvents(new onBedEnter(), this);
        pluginManager.registerEvents(new onBedLeave(), this);

        if (getConfig().getBoolean("Updater.Enabled")) {
            pluginManager.registerEvents(new Updater(), this);
        }
    }

    public static boolean isNight(World world) { return (world.getTime() > 12541 && world.getTime() < 23850); }

    public static boolean isVanished(Player p) {
        for (MetadataValue meta : p.getMetadata("vanished")) {
            if (meta.asBoolean()) return true;
        }
        if (p.getGameMode() == GameMode.SPECTATOR) {
            return true;
        }
        if (Essentials != null) {
            return Essentials.getUser(p).isVanished();
        }
        return false;
    }

    public static int getOnlinePlayers() {
        int count = 0;
        for (Player p : Bukkit.getOnlinePlayers()) {
            if (!isVanished(p)) {
                count++;
            }
        }
        return count;
    }

    public static boolean hasPlaceholderAPI() {
        return hasPlaceholderAPI;
    }

}
